<!DOCTYPE html>
<html>
<head>
	<title>Mencari Bilangan Prima</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container">
		
		<header>
			<h1 class="text-success">CONTOH PROGRAM PERCABANGAN</h1></br>
			<h1 class="text-success">MENCARI BILANGAN PRIMA</h1>
		</header>

		<div class="form">

			<div class="header-form col-md-12 btn-success">
				<h3 class="text-white">INPUT BILANGAN</h3>
			</div>

			<form action="hasil.php" method="POST">
				<div class="input-group">
					<input class="col-md-12 form-control" type="number" name="in_bilangan" placeholder="Masukkan bilangan ..." required>
					<input class="col-md-12 form-control text-white btn-success" type="submit" value="Submit">
				</div>
			</form>

		</div>
		
	</div>
</body>
</html>