<?php
    $bilangan = $_POST['in_bilangan'];
    $n = $bilangan;
    $hasil = $n." Adalah Bilangan Prima";

    if($n == 0 || $n == 1){
        $hasil = $n." Bukan Bilangan Prima";
    }
    else{
        for($i = 2; $i <= $n-1; $i++){
            if($n % $i == 0){
                $hasil = $n." Bukan Bilangan Prima";
                break;
            }
        }
    }
?>


<html>
    <head>
        <title>Program Pengecekan Bilangan Prima</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
    <div class="container"><br>
        <header>
        <h1 class="text-success">CONTOH PROGRAM PERCABANGAN</h1></br>
			<h1 class="text-success">MENCARI BILANGAN PRIMA</h1>
        </header><br>
        <main>
            <div class="form">

            <div class="header-form col-md-12 btn-success">
				<h3 class="text-white">HASILNYA</h3>
			</div><br>
                
            <form action="percabangan.php" method="POST">    
                <h3 class="text-success"><?php echo $hasil ?></h3>    
                <br>
                <div class="input-group">
					<input class="col-md-12 form-control text-white btn-success" type="submit" value="Cek lagi">
				</div>
            </form>
            <form action="../home.html" method="POST">
            <div class="input-group">
					<input class="col-md-12 form-control text-white btn-success" type="submit" value="KEMBALI KE HOME">
			</div>
            </form>    

            </div>   
            <div>

</div>
        </main>
    </div>
    </body>

</html>